# Monster Puncher

This was created in a mad dash with my homeboy [Adam](https://github.com/adamsmasher) at [TOJam 2009](http://tojam.ca), a game jam held yearly in Toronto.

There was coffee, fighting with mercurial commands, and sleeping under desks galore.

The finished game [travelled the country](http://handeyesociety.com/torontron/)!

## Requirements

[ TODO ]

## Running

[ TODO ]