class State
  def initialize
    @children = []
    @scripts = []
    @parent = nil
  end
  attr_writer :parent
  def update
    @scripts.each {|script| script.call}
    @scripts.delete_if {|script|
      script.respond_to?(:dead) and script.dead}      
    @children.each {|child| child.update}
  end
  def replace new_state
    die
    @parent.add_child new_state
  end
  def each_child &block
    @children.each do |child|
      yield child
      child.each_child &block
    end
  end
  def die
    @parent.remove_child self
  end
  def add_child child
    @children.push child
    child.parent = self
    self
  end
  def remove_child child
    @children.delete child
  end
  def add_script script
    @scripts.push script
    self
  end
end
