require 'sdl'

class Keyboard
  private_class_method :new
  @@keys_pressed_this_frame = []
  @@keys_released_this_frame = []

  def Keyboard.key_pressed key
    @@keys_pressed_this_frame.push key
  end

  def Keyboard.key_released key
    @@keys_pressed_this_frame.push key
  end

  def Keyboard.process_events key_events
    @@keys_pressed_this_frame = []
    @@keys_released_this_frame = []
    SDL::Key.scan

    key_events.each do |e|
      case e
      when SDL::Event::KeyDown
        @@keys_pressed_this_frame.push e.sym
      when SDL::Event::KeyUp
        @@keys_released_this_frame.push e.sym
      end
    end
  end

  def Keyboard.key_down? key
    SDL::Key.press? key 
  end

  def Keyboard.key_up? key
    not key_down? key
  end

  def Keyboard.key_pressed? key
    @@keys_pressed_this_frame.include? key
  end

  def Keyboard.key_released? key
    @@keys_released_this_frame.include? key
  end
end
