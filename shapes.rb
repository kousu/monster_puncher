require 'redbeard'
require 'sdl'
require 'things'

class Rectangle < Thing
  def initialize(x,y,w,h,color)
    super(x,y,w,h)
    extend Renderable $viewport
    @color = color
    redraw
  end

  def redraw
    @img = Screen.new_img(@w,@h)
    @img.fill_rect(0,0,@w,@h,@img.format.map_rgb(*@color))
  end

  def w= new_w
    @w = new_w
    redraw
  end

  def h= new_h
    @h = new_h
    redraw
  end
end

class Image < Thing
  attr_accessor :img
  
  def initialize(x,y,filename_or_surface)
    if filename_or_surface.class == SDL::Surface
      @img = filename_or_surface
    else
      @img = SDL::Surface.load(filename_or_surface)
    end
    super(x,y,@img.w,@img.h)
    extend Renderable
  end

  def flip_h
    self.img.flip_h
  end
  
  def flip_v
    (@img.h/2).times do |y|
      @img.w.times do |x|
        p1,p2 = @img.get_pixel(x,y),@img.get_pixel(x,@img.h-y)
        @img.put_pixel(x,y,p2)
        @img.put_pixel(x,@img.h-y,p1)
      end
    end
  end
end



class Circle < Thing
  def initialize(x,y,r,color)
    @r = r
    @d = @r*2+1
    super(x,y,@d,@d)
    extend Renderable
    @color = color
    redraw
  end
  
  def redraw
    @img = Screen.new_img(@d,@d)
    @img.fill_rect(0,0,@d,@d,@img.format.map_rgba(0,0,0,SDL::ALPHA_TRANSPARENT))
    @img.draw_circle(@r,@r,@r,@img.format.map_rgb(*@color),fill=true,aa=true)
  end
end

class Text < Thing
  def initialize(text, font_name, size, x, y, color)
    @font = SDL::TTF.open(font_name, size)
    w,h = @font.text_size(text)
    super x,y,w,h
    extend Renderable
    @text = text
    @color = color
    redraw
  end
  attr_reader :text
  
  def text=(new_text)
    @text = new_text
    redraw
    @w,@h = @font.text_size(text)
  end

  def redraw
    @img = @font.render_blended_utf8(@text, *@color)
  end
  
  attr_reader :img
end
