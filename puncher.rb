require 'redbeard'
require 'rendering'
require 'shapes'
require 'things'
require 'monster'
require 'titlescreen'
require 'enemies'
require 'clouds'
require 'screens'
require 'boss'

# the moon is the 'top'
# across all the regions are split MOON_ALTITUDE steps
MOON_ALTITUDE = 10000
#MOON_ALTITUDE = 2500
IRL_MOON_ALTITUDE=384_403 #kilometers

TERMINAL_VELOCITY = 8
$gRAVITY = 0.20 #if nil, goes into 'no grav' mode
DEBUG = false
MUSIC = true
ENEMIES = false
JUMP_SPEED = 8
SPIN_SPEED=Math::PI/4 / 2
SPIN_FRICTION = SPIN_SPEED/60 #larger makes the blades spin down faster
TOLERANCE=50
MAX_STARS=100

STARS_HEIGHT=MOON_ALTITUDE*0.75 #stars begin at 75% of the way up

ScreenWidth=360
ScreenHeight=640

SDL::init(SDL::INIT_AUDIO|SDL::INIT_VIDEO)
SDL::Mixer.open(44100/2, SDL::Mixer::DEFAULT_FORMAT, 2, 1024) if MUSIC #reopen the sound to set the sample rate correctly


IMAGES = {
  :shark_attack_l => "visual/shark_attack.png",
  :shark_attack_r => "visual/shark_attack.png",
  :lazar_shark => "visual/lazar_und_feests.png",
  :cloud1 => "visual/cloud1.png",
  :cloud2 => "visual/cloud2.png",
  :bass => "visual/bass.png",
  :piranha => "visual/piranha.png",
  :salmon => "visual/salmon.png",
  :monster => "visual/monster1.png",
  :fist => "visual/fist.png",
  :grass => "visual/grass.png",
  :moon => "visual/moon.png",
  :title => "visual/title.png",
}


IMAGES.each do |k,v|
  IMAGES[k] = SDL::Surface.load(v)
end
IMAGES[:shark_attack_l].flip_h

SOUNDTRACK = {
  :gameplay=>SDL::Mixer::Music.load("audio/ACTION1.ogg"),
  :boss=>SDL::Mixer::Music.load("audio/omnous.ogg"),
  :love=>SDL::Mixer::Music.load("audio/aww.ogg"),
}

SOUNDEFFECTS = {
  :helicopter=>SDL::Mixer::Wave.load("audio/helicopter.ogg"),
  :collide_enemy=>SDL::Mixer::Wave.load("audio/collide_enemy.ogg"),
  :collide_player=>SDL::Mixer::Wave.load("audio/collide_player.ogg"),
  :dead_player=>SDL::Mixer::Wave.load("audio/dead_player.ogg"),
  :boss_explode=>SDL::Mixer::Wave.load("audio/boss_explode.ogg"),
}

def soundeffect(e)
  SDL::Mixer.play_channel(1, SOUNDEFFECTS[e], 0)
  SDL::Mixer.set_volume(1, 78)
end

Level = build_level

class Puncher < State
  def initialize
    super
    @over = false
    @monster = Monster.new
    grass = Grass.new
    moon = Moon.new
    moon.x = SDL::Screen.get.w/2 - moon.w/2
    
    #sit the monster on top of the grass
    @monster.y = grass.y + @monster.h
    @monster.y = MOON_ALTITUDE*0.9 
    
    add_child @monster
    add_child grass
    add_child moon
       
    #pausing
    add_script(Proc.new do
      unless @over
                 if Keyboard.key_pressed? SDL::Key::P
                   if @paused
                     unpause
                     unless @_pause_state.nil?
                       @_pause_state.die
                       @_pause_state = nil
                     end
                   else
                     @_pause_state = Paused.new
                     $puncher.add_child @_pause_state
                     pause
                   end
                 end
               end
           end)
    
    t = Text.new("", "font.ttf", 25, 0, SDL::Screen.get.h - 25, [255,0,0])
    add_child t
    flash_text = Proc.new do
      altitude = ((@monster.y - @monster.h) - grass.y).to_i
      altitude = altitude.to_f/MOON_ALTITUDE * IRL_MOON_ALTITUDE
      t.text = "#{altitude.to_i} km!"
    end

    t2 = Text.new("", "font.ttf", 25, 0, ScreenHeight - 25, [255,0,0])
    add_child t2
    flash_text2 = Proc.new do
      t2.text = "#{$lives_left}"
      t2.x = ScreenWidth-t2.w
    end
    
    add_script flash_text
    add_script flash_text2
    
    run_level = Script.new do |this|
      Level.each do |info|
        y,pattern = info
        pass while $viewport.world_y - $viewport.h < y
        run_pattern this,pattern
      end
      # YOU WIN
    end

    add_script run_level.init(self)
    
    
    SDL::Mixer::halt(-1)
    sleep(0.1) #hack: insert a gap in the music to denote the transition into a new game (if there was an old game; otherwise this has no real effect)
    SDL::Mixer::play_music(SOUNDTRACK[:gameplay],-1)
  end

  def pause(music_too=true) #arg is a hack; covers the case where we want to pause the gameplay but keep the music going
    @paused = true
    each_child {|child| child.pause if child.respond_to? :pause }
    SDL::Mixer.pause_music  if MUSIC and music_too
  end

  def unpause
    @paused = false
    each_child {|child| child.unpause if child.respond_to? :unpause}
    SDL::Mixer.resume_music  if MUSIC
  end
  
  def game_over
    @over = true
    #SDL::Mixer.fade_out_music(7*1000)
    SDL::Mixer.halt_music
    add_child GameOver.new
    @monster.die
  end
  
  def die
    SDL::Mixer.halt_music
    super
  end
end

class Grass < Image
  def initialize
    @render_target = $viewport
    super(0,0,IMAGES[:grass])
    self.y = self.h
    extend Collidable
  end
  attr_reader :render_target
end

class Moon < Image
  def initialize
    @render_target = $viewport
    super(0,MOON_ALTITUDE,IMAGES[:moon])
    extend Collidable
  end
  attr_reader :render_target
end

Collision::on_collide(Monster,Moon) do |monster,moon|
  unless $fighting_boss
    $puncher.add_child(BossLevel.new moon)
    $fighting_boss = true
  end
  monster.dy = -1
  monster.eject_from moon
  monster.dy = 0
end
    

Collision::on_collide(Monster,Grass) do |monster,grass|
  monster.dy = 1
  monster.eject_from grass
  monster.dy = 0
end

class DeadThing < Image
  attr_reader :render_target
  
  def initialize base
  
    @render_target = $viewport
    
    super(base.x, base.y, base.img.copy)
#    extend Collidable
    @original_img = @img
    @angle = base.angle
    @speed = 8
    @rotation = 0
    
    
    add_script Proc.new {die if $viewport.off_screen? self}
    add_script(Proc.new do
                 @rotation += 12
                 @img = @original_img.transform_surface(@original_img.map_rgba(0,0,0,SDL::ALPHA_TRANSPARENT),@rotation,1,1,0)
               end)
               
    #now that we're adding movement this has to change...
    add_script Proc.new {    
      if $viewport.off_screen? self
        die
      end
    }
  end
end


$spinning = false


class Star < State
 #a star is a little dot of light
 #it is come to destroy your soul
 @@img = nil
 
 attr_accessor :x, :y
 attr_accessor :w,:h
 
 def initialize x,y,brightness=1
   super()
   @x = x
   @y = y
   @w = 1
   @h = 1
   
   if @@img.nil?
     @@img = Screen.new_img(1,1)
     @@img[0,0] = @@img.format.map_rgb(255,255,255)
   end
   
   add_script(Proc.new do
     $viewport.add_to_current_frame self
   end
   )
   
   add_script(Proc.new do
     #die if $viewport.off_screen? self
      #handled by the background instead
   end
   )
 end
 
 def render()
   [@@img, @x, @y, -1]
 end

end

NumOfRegions = 5
def current_regions
  return (($viewport.world_y - $viewport.h).to_f/MOON_ALTITUDE * 
          NumOfRegions).to_i
end

class Background < State
  RegionColors = [[100,100,100], #fog
                  [80, 160, 255], #blue sky
                  #orange
                  #this could be better picked
                  #the blue->orange transition goes through greyish
                  [255,132,12],
                  #purple night
                  [64,0,128],
                  [0,0,0] #space
                 #TODO: make these colours change slightly at random
                ]
  def initialize
    super
    add_script Proc.new {
      #the region we're in currently
      n = current_regions

      if n == 2
        CloudGenerator.max_clouds = 100
        CloudGenerator.cloud_rate = 0.8
      elsif n < 2
        CloudGenerator.max_clouds = 15
        CloudGenerator.cloud_rate = 0.01
      else
        CloudGenerator.max_clouds = 0
        CloudGenerator.cloud_rate = 0
      end

      bottom = (n < NumOfRegions ? RegionColors[n] : RegionColors.last)
      top = (n+1 < NumOfRegions ? RegionColors[n+1] : RegionColors.last)
      region_size = MOON_ALTITUDE.to_f/NumOfRegions
      top_location = (n+1)*region_size
      bottom_location = n*region_size
  
      position_in_this_region_as_a_ratio = ($viewport.world_y - $viewport.h - bottom_location)/region_size
      
      #tween between the two colours
      #weighted by how far between their absolute points we're at
      colour = [nil, nil, nil]
      colour.count.times do |i|
        colour[i] = bottom[i]+(top[i]-bottom[i])*position_in_this_region_as_a_ratio
        colour[i] = colour[i].to_i
        colour[i] = [0, [255, colour[i]].min].max
      end
      Screen.bg_color = *colour
    }
  
    @stars = []
    
    #stars
    add_script Proc.new {
      region_size = MOON_ALTITUDE.to_f/NumOfRegions
      stars_height = MOON_ALTITUDE - 1.65*region_size #start seeing stars 45% through the second-to-last region
      if $viewport.world_y > stars_height
        to_the_stars = ($viewport.world_y - stars_height)/(MOON_ALTITUDE - stars_height) #a percentage
        max_stars = to_the_stars * MAX_STARS #should this be logarithmic?
        alive_stars = []
        @stars.each do |star|
          if star.y < $viewport.world_y - $viewport.h - TOLERANCE
            star.die
          else
            alive_stars.push star
          end
        end
        @stars = alive_stars
        
        
        n = [(max_stars - alive_stars.count).to_i, 0].max
        n.times do #the number of stars that appear should be biased by how close to the top we are
          x = (rand ScreenWidth)
          y = $viewport.world_y + rand*$viewport.h
          @stars.push Star.new(x,y)
          add_child @stars.last
        end
      end
    }
  end
end


Collision::on_collide(Fist, Enemy) do |fist, enemy|
  soundeffect(:collide_enemy)
  
  # "move" everything over to the center
  dx = (enemy.x + enemy.w/2) - (fist.x + fist.w/2)
  dy = (enemy.y - enemy.h/2) - (fist.y - fist.h/2)

  #puts "dx dy #{dx} #{dy}"
  
  angle, = to_polar(dx,dy)

 # puts "ANGLE: #{angle}"

  dx2,dy2 = xy_components(angle,fist.w/2)

  #puts "dx2, dy2 #{dx2} #{dy2}"

  # get slope of tangent
  m = -dx2/dy2

  # get normal
  nm = (1/m).abs
  x_sign = (angle.between? Math::PI/2, 3*Math::PI/2) ? -1 : 1
  y_sign = (angle.between? 0, Math::PI) ? 1 : -1

#  puts "nm, xsign, ysign #{nm} #{x_sign} #{y_sign}"
#  x_reflect = (angle.between


  # send enemy along normal
  enemy.angle = to_polar(nm*x_sign,y_sign)[0]
  
  enemy.eject_from(fist)
  enemy.replace DeadThing.new enemy
end

def build_master_state()
  $viewport = Viewport.new(0,SDL::Screen.get.h,0,0,-100,SDL::Screen.get.w,SDL::Screen.get.h)
  puncher = Puncher.new
  puncher.add_child $viewport
  puncher.add_child Background.new
  puncher.add_child CloudGenerator.new #hack: add the cloud generator *after* pausing so that it's running during the title screen
  $num_of_clouds = 0 #hack!
  $fighting_boss = false
  $lives_left = 10
  puncher
end
  
$game = Game.create('MONSTER PUNCHER', ScreenWidth, ScreenHeight)
$game.add Proc.new {exit if Keyboard.key_pressed? SDL::Key::ESCAPE}
$puncher = build_master_state
$game.add $puncher
$game.add TitleScreen.new
$puncher.pause(false)
$game.run

