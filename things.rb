require 'redbeard'

class Thing < State
  def initialize(x,y,w,h)
    super()
    @x,@y,@w,@h = x,y,w,h
    @angle = 0
    @speed = 0
    @paused = false
    add_script Proc.new {self.move}
  end
  attr_accessor :x, :y, :w, :h, :angle, :speed, :destination

  def dx
    xy_components(@angle, @speed)[0]
  end

  def dy
    xy_components(@angle, @speed)[1]
  end

  def dx=(new_dx)
    @angle, @speed = to_polar(new_dx, self.dy)
  end

  def dy=(new_dy)
    @angle, @speed = to_polar(self.dx, new_dy)
  end

  def move
    unless @paused
      return if speed.zero? # not necessary, but faster than all that trig
      dx,dy = xy_components(angle, speed)
      self.x += dx
      self.y += dy
    end
  end

  def destination=(x,y)
    @destination = x,y
    @angle, = to_polar(self.x-x,self.y-y)
  end

  def eject_from obj
    unless @paused #XXX trouble brews here... what if we collide with something and pause in the same frame? then we will get multiple collisions..
      move while Collision::colliding? self, obj
    end
  end
  
  def pause
    @paused = true
  end

  def unpause
    @paused = false
  end
end

def xy_components(angle, speed)
  return speed*Math.cos(angle),speed*Math.sin(angle)
end
  
def to_polar(x,y)
  angle = Math.atan2(y,x)
  speed = Math.sqrt(x**2 + y**2)
  return angle,speed
end

Up = Math::PI/2
Down = Math::PI*3/2
Left = Math::PI
Right = 0
