class DispatchTable
  def initialize
    @methods = {}
  end

  def add(method_name,c1,c2,method)
    dispatch_table = (@methods[method_name] or @methods[method_name] = {})
    if dispatch_table[c1] then dispatch_table[c1][c2] = method
    else dispatch_table[c1] = {c2=>method} end
  end      

  def lookup(method_name,obj1,obj2)
    dispatch_table = @methods[method_name]
    return nil if not dispatch_table
    c1 = obj1.class
    while c1
      c1_methods = dispatch_table[c1]
      if c1_methods
        c2 = obj2.class
        while c2
          method = c1_methods[c2]
          return method if method
          c2 = c2.superclass
        end
      end
      c1 = c1.superclass
    end
    return nil
  end
end

class Collision
  private_class_method :new
  @@objs = []
  @@collision_table = DispatchTable.new

  def Collision.check_this_frame obj
    @@objs.push obj
  end

  def Collision.on_collide(c1,c2,&proc)
    @@collision_table.add(:collide,c1,c2,proc)
  end

  def Collision.handle_collisions
    while obj1 = @@objs.shift
      @@objs.each {|obj2| check_for_collision(obj1,obj2)}
    end    
  end

  def Collision.check_for_collision obj1, obj2    
    if colliding? obj1, obj2
      method = @@collision_table.lookup(:collide,obj1,obj2)
      return method.call(obj1,obj2) if method
      
      method = @@collision_table.lookup(:collide,obj2,obj1)
      return method ? method.call(obj2,obj1) : nil
    end
  end

  def Collision.colliding? obj1, obj2
    left1, left2 = obj1.x, obj2.x
    right1, right2 = obj1.x + obj1.w, obj2.x + obj2.w
    top1, top2 = obj1.y, obj2.y
    bottom1, bottom2 = obj1.y - obj1.h, obj2.y - obj2.h
    return (right1 > left2 and
            left1 < right2 and
            bottom1 < top2 and
            top1 > bottom2)
  end
end

module Collidable
  def self.extended obj
    obj.add_script(Proc.new do
                     Collision::check_this_frame obj
                   end)
  end
end
