require 'redbeard'
require 'things'

class TitleScreen < State
  def initialize
    super()
    title = Image.new(0,0, "visual/title.png")
    title.x, title.y = SDL::Screen.get.w/2-title.w/2, SDL::Screen.get.h*0.15 #center it
    add_child title
    
    flash_text = Script.new do |this|
      loop {
        t = Text.new("PRESS SPACE (a lot)", "font.ttf", 24, 0, SDL::Screen.get.h*0.45, [0xAA,0xAA,0x55])
        t.x = SDL::Screen.get.w/2-t.w/2
        this.add_child t
        wait 30
        this.remove_child t
        wait 30
      }
    end
    
    wait_for_start = Proc.new do
      if Keyboard.key_pressed? SDL::Key::SPACE        
        self.die
        $puncher.unpause
      end
    end
    
    add_script flash_text.init(self)
    add_script wait_for_start
  end
end
