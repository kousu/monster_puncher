


$num_of_clouds = 0 # hack!

class Cloud < Image
  def initialize z
    $num_of_clouds = $num_of_clouds + 1
    @render_target = $viewport
    
    side = (rand(2) == 0)
    y = nil
    if side
      y = $viewport.world_y-rand*SDL::Screen.get.h/2        
    elsif
      y = $viewport.world_y+rand*SDL::Screen.get.h
    end
    
    super(0, y, IMAGES[[:cloud1, :cloud2].sample])
    
    @img.set_color_key(SDL::SRCCOLORKEY,@img.format.map_rgb(255,0,255))
    @img.set_alpha(SDL::SRCALPHA,192) if z > 0

    going_left = rand 2
    if side
      if going_left.zero?
        self.x -= self.w
      else self.x = SDL::Screen.get.w end
    else
      self.y += self.h
      self.x = SDL::Screen.get.w*rand-SDL::Screen.get.w/2 #-self.w
    end
    @angle = Math::PI*going_left
    @speed = 0.5
    @z = z

    add_script Proc.new {
      if  $viewport.screen_x(self) > ScreenWidth or
          $viewport.screen_x(self) < -self.w or
          $viewport.screen_y(self) > ScreenHeight*1.5
        $num_of_clouds -=1
        die 
      end
    }
  end
  attr_reader :render_target
  
  def Cloud.Create_at(x,y,z)
    #normally a cloud spawns at a random location above the screen
    cloud = Cloud.new z
    cloud.x, cloud.y = x, y
    return cloud
  end
end

class CloudGenerator < State
  @@cloud_rate = 0.1
  @@max_clouds = 15

  def initialize
    super

    @@cloud_rate = 0.1
    @@max_clouds = 15

    add_child Cloud.Create_at(SDL::Screen.get.w/2*rand, SDL::Screen.get.h/2*rand+SDL::Screen.get.h/2,-2)
    add_child Cloud.Create_at(SDL::Screen.get.w/2*rand, SDL::Screen.get.h/2*rand+SDL::Screen.get.h/2, 2)
  
    add_script Proc.new {
      add_child Cloud.new 2 if rand < @@cloud_rate and $num_of_clouds < @@max_clouds
      add_child Cloud.new -2 if rand < @@cloud_rate and $num_of_clouds < @@max_clouds
    }
  end

  def CloudGenerator.cloud_rate=(new_val)
    @@cloud_rate = new_val
  end

  def CloudGenerator.max_clouds=(new_val)
    @@max_clouds = new_val
  end  
end
