require 'redbeard'

$fighting_boss = false
$won_the_game = false

FADE_TIME = 3
class BossLevel < State
  def initialize moon
    super()
    script = Script.new do |this, moon|
      fADE_TIME = 3
      $viewport.lock
      SDL::Mixer.fade_out_music(FADE_TIME*1000)
      
      
      moon.dy = 1
      pass while $viewport.on_screen? moon
      moon.dy = 0
      
      wait fADE_TIME*60 #wait for the old track to fade out
      wait 5*60 #let the new track come in a bit
      this.add_child SharkAttack.new
      
      die
    end

    add_script script.init(self,moon)
    
    add_script(Script.new do 
      if not SDL::Mixer.play_music?
        SDL::Mixer.play_music(SOUNDTRACK[:boss], -1) #this hangs if anything is still playing, so don't let it
      end
     end)
   end
end

class BossState < Image
  def initialize x,y,filename
    @render_target = $viewport
    super
    @img
    extend Collidable
    add_script Proc.new {die if $viewport.off_screen?(self,ScreenHeight)}
  end
  attr_reader :render_target
end
   
Collision::on_collide(Monster,BossState) do |monster, boss|
  #monster.take_damage
  #TEMP TEMP TEMP:
  boss.dy = -7
  boss.dx = 0
  $won_the_game = true
  
  explode_transition = Script.new do
    boss.replace GameWin.new
    $gRAVITY = 0
    monster.dy = -2
    #$viewport.unlock
    soundeffect(:boss_explode)
    wait(30)
    soundeffect(:boss_explode)
    wait(30)
    soundeffect(:boss_explode)
  end
  
  
  boss.add_script explode_transition
end
  

class SharkAttack < BossState

  def initialize
    facing_left = rand(2) == 0
    super 0,MOON_ALTITUDE-ScreenHeight+200, IMAGES[facing_left ? (:shark_attack_l) : (:shark_attack_r)]
    if facing_left
      self.x = ScreenWidth + self.w/2
    else
      self.x = -self.w/2
    end
    self.dx = 16
    self.dx = -self.dx if facing_left
    
    self.dy = 8
    add_script Proc.new {
      self.dy -= $gRAVITY if self.dy > -TERMINAL_VELOCITY}
    add_script Proc.new {
     unless $won_the_game
       self.replace next_attack.new if self.x > ScreenWidth + self.w or self.x < -self.w
     end
     }
  end
end

class LazarShark < BossState
  def initialize
    super 0,MOON_ALTITUDE, IMAGES[:lazar_shark]
    self.x = ScreenWidth/2-self.w/2
    self.y += self.h

    entrance = Script.new do |this|   
      this.dy = -4
      pass while this.y > MOON_ALTITUDE
      this.dy = 0
      wait 60 # INSERT LAZAR HEAR
      this.replace next_attack.new
      die
    end

    add_script entrance.init(self)
  end
end
    

def next_attack
  return [SharkAttack, LazarShark].sample
end
