require 'redbeard'

class Monster < Image
  def initialize
    @render_target = $viewport
    super(0,0,IMAGES[:monster]) #0,0 since the next line sets it
    self.y = self.h
    self.x = SDL::Screen.get.w/2 - self.w/2
    extend Collidable
    @punching = false
    @rot_speed = 0
    @invinsible = false
    
    #HACK
    #this is buggy, we should be able to say Right=0 here but because
    #inside Fist this is passed into an orthogonal coordinate system it's
    #kind of fucked
    @fist_angle = Math::PI/2 

    #HACK..
    #the angle of fist1 relative to our center (NB! center! not topleft!)
    #fist2's position can be derived from this    
    self.dy = -1 unless $gRAVITY.nil? rescue 0

    # left-right control
    add_script(Proc.new do
                 unless not DEBUG and @paused 
                   if Keyboard.key_down? SDL::Key::LEFT
                     self.x -= 4
                   elsif Keyboard.key_down? SDL::Key::RIGHT
                     self.x += 4
                   end
                 end
               end)
               
    #keep the player in the bounds
    add_script(Proc.new do
                 unless not DEBUG and @paused 
                   if self.x < 0                     
                     self.x = 0
                   elsif self.x > ScreenWidth-self.w
                     self.x = SDL::Screen.get.w-self.w
                   end
                 end
               end)

    #gravity
    add_script(Proc.new do
                 unless false or @paused
                   self.dy -= $gRAVITY and self.dy > -TERMINAL_VELOCITY
                 end
               end)
    
    #adjust the fists
    add_script(Proc.new do
                 if @punching and not @paused
                   #TODO
                   #play with putting this line after setting @rot_speed
                   @fist_angle += @rot_speed
                   @rot_speed -= SPIN_FRICTION
                   
                   @fist1.angle = @fist_angle
                   @fist2.angle = @fist_angle + Math::PI
                   
                   if @rot_speed < 0
                     @punching = false
                     @rot_speed = 0
                     @fist_angle = 0 #clamp it
                   end
                 end
               end)
   
    #adjust the current altitude to be roughly in step with the height of
    #the player
    add_script(Proc.new do
                 if $viewport.screen_y(self) < SDL::Screen.get.h*0.25
                   $viewport.move_obj_to self, SDL::Screen.get.h*0.25
                 elsif $viewport.screen_y(self) > SDL::Screen.get.h*0.5
                   $viewport.world_y -= 4
                 end
                 $viewport.world_y = 
                   [$viewport.world_y, MOON_ALTITUDE].min
               end)
    
    #check for FAIL
    add_script(Proc.new do
         unless $won_the_game
                 if $viewport.screen_y(self) > SDL::Screen.get.h
                   # this also calls .die on this
                   $puncher.game_over unless DEBUG
                 end
          end
               end)

    # helicopter punch
    @fist1 = Fist.new(self,Math::PI/2,1.25)
    @fist2 = Fist.new(self,Math::PI*3/2,1.25)
    @fist1.angle = @fist_angle
    @fist2.angle = @fist_angle + Math::PI
    add_child @fist1
    add_child @fist2
    add_script(Proc.new do
                 unless @paused
                   if Keyboard.key_pressed? SDL::Key::SPACE
                     self.punch
                   end
                 end
               end)
  end
  attr_reader :render_target
  attr_accessor :invinsible
  
  def punch
    unless $spinning
    @punching = true
    #a note: when you press jump. you go at *top speed* *immediately*
    #this is the 'feel' that we decided on.
    soundeffect(:helicopter)
    self.dy = JUMP_SPEED
    @rot_speed = SPIN_SPEED
    end
  end

  def take_damage
    falling_monster = nil
    unless self.invinsible
      soundeffect(:collide_player)
      $lives_left -= 1
      falling_monster = FallingMonster.new self
      self.replace falling_monster
      if $lives_left <= 0
        falling_monster.do_not_recover
        $puncher.game_over
        self.die
      end
    end

  end  

  def make_temp_invinsible
    s = Script.new do |this|
      this.invinsible = true
      wait 10
      this.invinsible = false
      die
    end

    add_script s.init(self)
  end
end




# this is what's used after you're hit
class FallingMonster < Image
  attr_reader :render_target
  def initialize monster
    @render_target = $viewport
    super monster.x,monster.y,monster.img

    monster.x,monster.y = -100, -100

    self.angle = Down
    self.dy = -10
    rot_angle = 0
    speed = 20
    @no_recovery = false

    original_img = monster.img.copy

    rotation = Script.new do |this|
      if speed > 0
        rot_angle += 12
        speed -= 0.6
        this.img = original_img.transform_surface(monster.img.format.map_rgba(0,0,0,SDL::ALPHA_TRANSPARENT),rot_angle,1,1,0).copy
      elsif not this.no_recovery
        monster.x = this.x
        monster.y = this.y
        monster.make_temp_invinsible
        this.replace monster
      end
    end
    add_script rotation.init(self)
  end
  attr_reader :no_recovery

  def do_not_recover
    @no_recovery = true
  end
end




class Fist < Image
  def initialize(monster, angle, distance)
    #angle is the initial angle to be at
    #distance is the initial *multiple* of the width to be from the center

    @render_target = $viewport #needs to be before super()
    super(monster.x,monster.y,IMAGES[:fist])
    extend Collidable    
    @monster = monster
    @angle = angle
    #the distance that our center is from the center of the monster
    @distance = self.w*distance
    
    @left_img = @img.display_format_alpha
    flip_h
    @right_img = @img.display_format_alpha
    
    adjust #avoid an ugly jump after the first frame renders
    add_script Proc.new {adjust}    
  end
  
  attr_reader :render_target
  attr_accessor :z
  attr_accessor :angle
  attr_accessor :distance #allow external things to move us about
  
  def adjust
    #move ourselves to follow the player and where our angle has moved to
    old = [self.x, self.y, self.z]
    
    #relative to the monster, judging from the *centers* of both 
    self.x = (@monster.x + 
              (@monster.w/2 - self.w/2) + 
              @distance*Math::sin(@angle))
    self.y = @monster.y - 16
    #we use sin() because the circle angle refers to is orthogonal to the 
    #monster so y==z
    self.z = @distance*Math::cos(@angle) <=> 0 

    if x + w/2 < (@monster.x + @monster.w/2)
      @img = @left_img
    else
      @img = @right_img
    end
  end
end
