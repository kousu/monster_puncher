require 'redbeard'
require 'shapes'

class Enemy < Image
  def initialize x,y,filename
    @render_target = $viewport
    super
    @img
    extend Collidable
    add_script Proc.new {die if $viewport.off_screen?(self,ScreenHeight)}
  end
  attr_reader :render_target
end

class Bass < Enemy
  def initialize y,from_the_right
    super(0,y,IMAGES[:bass])
    flip_h if from_the_right
    self.x = (from_the_right ? SDL::Screen.get.w : -self.w)
    self.dx = 10# + (rand 40)
    self.dx = -self.dx if from_the_right
  end
end

class Piranha < Enemy
  def initialize y,from_the_right
    super(0,y,IMAGES[:piranha])
    flip_h if from_the_right
    self.x = (from_the_right ? SDL::Screen.get.w : -self.w)
    self.dx = 2
    self.dx = -self.dx if from_the_right
    self.dy = 2
    add_script Proc.new {self.dy -= $gRAVITY if self.dy > -TERMINAL_VELOCITY}
  end
end

class Salmon < Enemy
  def initialize x, from_the_bottom
    super(x,0,IMAGES[:salmon])
    flip_v if from_the_bottom
    self.y = (from_the_bottom ? $viewport.world_y - SDL::Screen.get.h : $viewport.world_y + self.h)
    self.dy = -8
    self.dy = -self.dy if from_the_bottom
  end
end
    

def make_piranha_fountain base
  piranha_fountain = []
  20.step(ScreenHeight/4*3,120) do |y|
    piranha_fountain.push [Piranha,0,base+y,false]
    piranha_fountain.push [Piranha,0,base+y,true]
  end
  
  return piranha_fountain
end

def make_bass_arrows base
  right = false
  bass_arrows = []
  bottom = ScreenHeight*3/4
  3.times do |n|
    y = bottom+n*300
    7.times {bass_arrows.push [Bass,5,base+y,right]}
    bass_arrows.push [Bass,5,base+y,right]
    right = !right
  end
  
  return bass_arrows
end
  

def build_level
  # we say a Level is a list of [y, pattern]
  # where a pattern is a list of [enemy_type, delay, *args]

  # let's make some patterns!
  l_to_r_salmon_top = []
  0.step(ScreenWidth, 80) do |x|
    l_to_r_salmon_top.push [Salmon, 30, x, false]
  end

  r_to_l_salmon_bottom = []
  ScreenWidth.step(0, -80) do |x|
    r_to_l_salmon_bottom.push [Salmon,30, x, true]
  end

  tube_of_salmon = []
  10.times do
    tube_of_salmon.push [Salmon,0,ScreenWidth/2-48,false]
    tube_of_salmon.push [Salmon,10,ScreenWidth/2+48,false]
  end

  return [] unless ENEMIES

  return [[ScreenHeight, l_to_r_salmon_top],
          [ScreenHeight*2, r_to_l_salmon_bottom],
          [ScreenHeight*3, tube_of_salmon],
          [ScreenHeight*4, make_piranha_fountain(ScreenHeight*5)],
          [ScreenHeight*5, make_bass_arrows(ScreenHeight*5)],
          [ScreenHeight*5+200, r_to_l_salmon_bottom]]
end

def run_pattern game, pattern
  pattern.each do |enemy_info|
    enemy_type,delay,*args = enemy_info
    game.add_child enemy_type.new(*args)
    wait delay 
  end
end

Collision::on_collide(Enemy, Monster) do |enemy, monster|
  monster.take_damage
end
