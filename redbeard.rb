require 'sdl'
require 'continuation'
require 'rendering'
require 'input'
require 'collision'

$frames = 0

class Game
  private_class_method :new
  @@instance = nil
  
  def initialize(name,w,h)
    SDL.init(SDL::INIT_VIDEO)
    SDL::TTF.init
    @name = name
    @root = State.new
    Screen.init(w,h)
  end

  def Game.create(name,w,h)
    @@instance = new(name,w,h) unless @@instance
    @@instance
  end

  def add state_or_script
    case state_or_script
    when State
      @root.add_child state_or_script
    when Script
      @root.add_script state_or_script
    when Proc
      @root.add_script state_or_script
    else
      throw "Invalid argument to Game.add!"
    end
  end

  def process_events
    keyboard_events = []
    while event = SDL::Event.poll
      case event
      when SDL::Event::KeyUp, SDL::Event::KeyDown
        keyboard_events.push event
      when SDL::Event::Quit
        exit
      end
    end
    Keyboard.process_events keyboard_events
  end

  def fps_on_quit
    start_time = Time.now.to_i
    at_exit do
      end_time = Time.now.to_i
      total_time = end_time - start_time
      unless total_time == 0
        fps = $frames/total_time 
        puts "Ran for: #{total_time}"
        puts "FPS: #{fps}"
      end
    end
  end

  def run
    SDL::WM.set_caption(@name, "")
    fps_on_quit
    next_frame = SDL.get_ticks + 17
    loop do
      process_events
      @root.update
      Collision::handle_collisions
      Screen.render
      $frames += 1
      SDL.delay(1) while SDL.get_ticks < next_frame
      next_frame = SDL.get_ticks + 17
    end
  end
end

class Script
  def initialize(&script)
    @script = script
    @return_point = nil
    @dead = false
  end
  attr_writer :return_point
  attr_reader :dead

  def init (*args)
    @args = args
    self
  end

  def die
    @dead = true
  end

  def call
    $current_script = self
    callcc do |call_site|
      $call_site = call_site
      if @return_point then @return_point.call call_site
      else instance_exec(*@args,&@script)
      end
    end
  end
end

def pass
  callcc do |return_point|
    $current_script.return_point = return_point
    $call_site.call
  end
end

def wait n
  n.times {pass}
end
