require 'redbeard'
require 'state'

# --the render protocol--
# all objects which are said to be renderable must have a method
# render => [SDL::Surface,x,y,z]
# this should not be confused with the Renderable mixin, which is a convenience
# mixin that automatically registers the object to be rendered each frame and
# provides a default render method

# an Output is a renderable object that queues up objects, then when the time comes renders them
# Screens and Viewports are examples of outputs, and probably the only good cases for them
# so generally users shouldn't make them - they should just use the Screen/Viewports
class Output < State
  def initialize x,y,z,w,h
    super()
    @x,@y,@z,@w,@h = x,y,z,w,h
    @to_render = []
  end
  attr_reader :x, :y

  def add_to_current_frame obj
    @to_render.push obj
  end

  def generate_renderables
    @to_render.map! {|obj|
      obj.render}
  end

  def process_renderables
    @to_render.insertion_sort! do |info1,info2|
      z1 = info1[3]
      z2 = info2[3]
      z1 <=> z2
    end
  end

  def render
    generate_renderables
    process_renderables
    @to_render.each do |info|
      img_buf,x,y,z = info
      Screen.blit_surface(img_buf,x,y)
    end
    @to_render = []
    return @img,@x,@y,@z
  end
end

class Viewport < Output
  def initialize world_x,world_y,screen_x,screen_y,z,w,h
    super screen_x,screen_y,z,w,h
    @world_x, @world_y = world_x, world_y
    @locked = false
    add_script Proc.new {Screen.add_to_current_frame self}
  end
  attr_accessor :world_y,:world_x
  attr_reader :h

  def locked?
    return @locked
  end

  def lock
    @locked = true
  end

  def unlock
    @locked = false
  end

  def process_renderables
    super
    @to_render.map! do |info|
      img,x,y,z = info
      [img,x+@world_x,@world_y-y,z]
    end
  end

  def on_screen? obj,tolerence=0
    screen_x(obj).between?(-obj.w-tolerence,@w+tolerence) and screen_y(obj).between?(-obj.h-tolerence,@h+tolerence)
  end

  def off_screen? obj,tolerence=0
    not on_screen? obj,tolerence
  end

  def move_obj_to obj,y
    self.world_y = y + obj.y
  end

  def world_y=(val)    
    @world_y = [@h,val].max unless self.locked?
  end

  def screen_x obj
    obj.x+@world_x
  end

  def screen_y obj
    @world_y-obj.y
  end
  
  def center_on obj
    self.world_y = obj.y + @h/2
  end
end

class Array
  def insertion_sort! &block
    block = Proc.new {|one,two| one <=> two} if not block
    i = 1
    while i < self.length
      # remove the element
      elem = self.delete_at i
      # insert each element in its correct place
      i2 = 0
      i2 += 1 while i2 < i and block.call(self[i2], elem) < 0
      self.insert(i2,elem)
      i += 1
    end
  end
end

# The Screen is a singleton Output
# objects within the world should render to a viewport
# but static objects (Viewports, HUDs) can render directly to it
class Screen < Output
  private_class_method :new
  @@instance = nil

  def initialize w,h,bg_color=[0,0,0]
    super 0,0,0,w,h
    @img = SDL::Screen.open(@w,@h,32,SDL::HWSURFACE|SDL::DOUBLEBUF)
    @bg_color = bg_color
  end
  attr_accessor :bg_color

  def Screen.init w,h
    throw "Screen already initialized" if @@instance
    @@instance = new(w,h)
  end

  def blit_surface(src,x,y)
    SDL::Surface.blit(src,0,0,0,0,@img,x,y) if src
  end

  def render
    @img.fill_rect(0,0,@w,@h,@img.format.map_rgb(*bg_color))    
    super
    @img.flip
  end

  def new_img(w,h)
    SDL::Surface.new(SDL::SWSURFACE|SDL::SRCALPHA, w, h, @img.format).display_format_alpha
  end

  def Screen.method_missing(m, *args)
    throw "Screen not initialized!" if not @@instance
    @@instance.send(m,*args)
  end
end

class SDL::Surface
  def copy
    self.display_format_alpha
  end
  
  def flip_h
    SDL::Surface.auto_lock_off
    self.lock
    self.h.times do |y|
      (self.w/2).times do |x|
        p1,p2 = self.get_pixel(x,y), self.get_pixel(self.w-x,y)
        self.put_pixel(x,y,p2)
        self.put_pixel(self.w-x,y,p1)
      end
    end
    self.unlock
    SDL::Surface.auto_lock_on
  end
end

module Renderable
  attr_reader :img

  def self.extended obj    
    target = obj.respond_to?(:render_target) ? obj.render_target : Screen
    obj.add_script Proc.new {target.add_to_current_frame obj}
  end
    
  def render
    return self.img, self.x, self.y, (self.instance_variable_defined?(:@z) ? @z : 0)
  end
end
