class GameOver < State
  def initialize
    super
    game_over = Text.new("Game Over", "font.ttf", 14, SDL::Screen.get.w/2, SDL::Screen.get.h*0.45, [255,255,255]) #Image.new(0,0, "visual/title.png")
    game_over.x -= game_over.w/2
    add_child game_over
    
    
    ffffff = Script.new do |this|
      wait (60*1.5).to_i #pause so that you don't restart immediately if you're kngeyboard mashi
      loop {
       if Keyboard.key_pressed? SDL::Key::SPACE
      	$puncher.die
        $puncher = build_master_state
        $game.add $puncher
        this.die
        break
      end
      pass #hand control back to the rest of the game
      }
      die
    end
    
    add_script ffffff.init(self)
    
    soundeffect(:dead_player)
  end
end

class GameWin < State
  def initialize
    super
    game_over = Text.new("Game Win", "font.ttf", 14, SDL::Screen.get.w/2, SDL::Screen.get.h*0.45, [255,255,255]) #Image.new(0,0, "visual/title.png")
    game_over.x -= game_over.w/2
    add_child game_over
    
    
    ffffff = Script.new do |this|
      wait (60*1.5).to_i
      loop {
      
      pass #hand control back to the rest of the game
      }
      die
    end
    
    add_script ffffff.init(self)
    
    SDL::Mixer.play_music(SOUNDTRACK[:love], -1)
  end
end

class Paused < State
  def initialize
    super
    
    blinken = Script.new do |this|
      t = Text.new("Paused", "font.ttf", 14, 0, SDL::Screen.get.h*0.45, [255,255,255])
      t.x = SDL::Screen.get.w/2-t.w/2
      loop {
        this.add_child t
        wait 30
        this.remove_child t
        wait 30
      }
      end
    add_script(blinken.init(self))
      
  end
  
end
